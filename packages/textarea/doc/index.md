## Textarea 文本域
<h5 style="color: #66d476">只有找到属于自己的世界人生才有意义。</h5>

<script setup>
    import BasicDemo from '../demo/basic_demo.vue'
    import DisabledDemo from '../demo/disabled_demo.vue'
    import MaxLengthDemo from '../demo/max_length_demo.vue'
    import ReadonlyDemo from '../demo/readonly_demo.vue'
    import SizeDemo from '../demo/size_demo.vue'
    import ResizeDemo from '../demo/resize_demo.vue'
    import Preview from '../../../src/components/preview.vue'
</script>

### 基本用法
<h6 style="color: #ffcf3f">文本域的基本用法</h6>
<BasicDemo />
<Preview comp="textarea" demo="basic_demo"/>

### 禁用状态
<h6 style="color: #ffcf3f">不可用的状态</h6>
<DisabledDemo comp="textarea" demo="disabled_demo"/>
<Preview comp="textarea" demo="disabled_demo"/>

### 字数限制
<h6 style="color: #ffcf3f">不可以输入那么多～</h6>
<MaxLengthDemo />
<Preview comp="textarea" demo="max_length_demo"/>

### 只读状态
<h6 style="color: #ffcf3f">给你看看就好</h6>
<ReadonlyDemo />
<Preview comp="textarea" demo="readonly_demo"/>

### 改变尺寸
<h6 style="color: #ffcf3f">可调节的尺寸</h6>
<ResizeDemo />
<Preview comp="textarea" demo="resize_demo"/>

### 固定尺寸
<h6 style="color: #ffcf3f">可调节的尺寸</h6>
<SizeDemo />
<Preview comp="textarea" demo="size_demo" />
