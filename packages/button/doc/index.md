## Button 按钮
<h5 style="color: #66d476">冒着掉眼泪的风险，2024 没联系方式的爱你。</h5>

<script setup>
    import BasicDemo from '../demo/basic_demo.vue'
    import DilsabledDemo from '../demo/disabled_demo.vue'
    import RoundDemo from '../demo/round_demo.vue'
    import SecondaryDemo from '../demo/secondary_demo.vue'
    import SizeDemo from '../demo/size_demo.vue'
    import preview from '../../../src/components/preview.vue'
</script>

### 基本用法
<h6 style="color: #ffcf3f">按钮的基本用法 啊吧啊吧～</h6>
<BasicDemo />
<preview comp="button" demo="basic_demo" />

### 次要按钮
<h6 style="color: #ffcf3f">看起来像果冻</h6>
<SecondaryDemo />
<Preview comp="button" demo="secondary_demo"/>

### 尺寸
<h6 style="color: #ffcf3f">有default、small、large三种尺寸</h6>
<SizeDemo />
<Preview comp="button" demo="size_demo"/>

### 禁用状态
<h6 style="color: #ffcf3f">不许点！</h6>
<DilsabledDemo />
<Preview comp="button" demo="disabled_demo"/>

### 圆角按钮
<h6 style="color: #ffcf3f">圆圆的也很可爱</h6>
<RoundDemo />
<Preview comp="button" demo="round_demo"/>
