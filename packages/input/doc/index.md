## Input 输入框
<h5 style="color: #66d476">人最重要的是找到属于自己的世界。</h5>

<script setup>
    import BasicDemo from '../demo/basic_demo.vue'
    import DisabledDemo from '../demo/disabled_demo.vue'
    import RoundDemo from '../demo/round_demo.vue'
    import PasswordDemo from '../demo/password_demo.vue'
    import SizeDemo from '../demo/size_demo.vue'
    import Preview from '../../../src/components/preview.vue'
</script>

### 基本用法
<h6 style="color: #ffcf3f">输入框的基本用法</h6>
<BasicDemo />
<Preview comp="input" demo="basic_demo" />

### 禁用状态
<h6 style="color: #ffcf3f">输入框输入可以禁用</h6>
<DisabledDemo />
<Preview comp="input" demo="disabled_demo" />

### 尺寸
<h6 style="color: #ffcf3f">可以选择四种尺寸的输入框</h6>
<SizeDemo />
<Preview comp="input" demo="size_demo" />

### 圆角
<h6 style="color: #ffcf3f">看起来也不错</h6>
<RoundDemo />
<Preview comp="input" demo="round_demo" />

### 输入密码
<h6 style="color: #ffcf3f">看起来也不错</h6>
<PasswordDemo />
<Preview comp="input" demo="password_demo" />
