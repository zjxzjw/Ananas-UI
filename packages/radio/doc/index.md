## 单选
<h5 style="color: #66d476">苦酒折柳今相离，无风无月也无你。</h5>

<script setup>
    import BasicDemo from '../demo/basic_demo.vue'
    import DisabledDemo from '../demo/disabled_demo.vue'
    import DefaultPoint from '../demo/default_point_demo.vue'
    import CustomColor from '../demo/custom_color_demo.vue'
    import ArrayDemo from '../demo/array_demo.vue'
    import preview from '../../../src/components/preview.vue'
</script>

### 基本用法
<BasicDemo />
<preview comp="radio" demo="basic_demo"/>

### 禁用状态
<h6 style="color: #ffcf3f">不许用！</h6>
<DisabledDemo />
<preview comp="radio" demo="disabled_demo"/>

### 自定义颜色
<h6 style="color: #ffcf3f">绿色是春天的颜色</h6>
<CustomColor />
<preview comp="radio" demo="custom_color_demo"/>

### 排列
<h6 style="color: #ffcf3f">提供了两种排列方式</h6>
<ArrayDemo />
<preview comp="radio" demo="array_demo"/>

### 默认选择
<h6 style="color: #ffcf3f">默默的选择了你</h6>
<DefaultPoint />
<preview comp="radio" demo="default_point"/>
