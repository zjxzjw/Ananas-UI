import AnButton from './button/index.js'
import AnInput from './input/index.js'
import AnRadio from './radio/index.js'
import AnTextarea from './textarea/index.js'

const components = [
    AnButton,
    AnInput,
    AnTextarea,
    AnRadio
]

const install = app => {
    components.forEach(item => {
        app.use(item)
    })
}

const AnanasUI = {
    install
}

export { AnButton }
export default AnanasUI