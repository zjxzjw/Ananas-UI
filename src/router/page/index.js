export default [
    {
        path: '/',
        name: 'Home',
        component: () => import('../../views/home.vue'),
        children: [
            {
                path: '',
                name: 'Ananas UI',
                component: () => import('../../../packages/ananas/doc/index.md')
            },
            {
                path: '/update',
                name: '更新日志',
                component: () => import('../../../packages/update/doc/index.md')
            },
            {
                path: '/install',
                name: '安装',
                component: () => import('../../../packages/install/doc/index.md')
            },
            {
                path: '/fast',
                name: '快速上手',
                component: () => import('../../../packages/fast/doc/index.md')
            },
            {
                path: '/button',
                name: 'Button 按钮',
                component: () => import('../../../packages/button/doc/index.md')
            },
            {
                path: '/radio',
                name: 'Redio 单选',
                component: () => import('../../../packages/radio/doc/index.md')
            },
            {
                path: '/input',
                name: 'Input 输入框',
                component: () => import('../../../packages/input/doc/index.md')
            },
            {
                path: '/textarea',
                name: 'Textarea 文本域',
                component: () => import('../../../packages/textarea/doc/index.md')
            },
        ]
    },
    {
        path: '/ananas',
        name: '',
        component: () => import('../../views/index.vue')
    },
]
