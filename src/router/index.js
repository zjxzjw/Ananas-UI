import { createRouter, createWebHashHistory } from 'vue-router'
import page from './page/index.js'

const router = createRouter({
  history: createWebHashHistory(import.meta.env.BASE_URL),
  routes: [...page]
})

export default router
