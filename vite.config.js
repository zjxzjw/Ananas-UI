import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import Markdown from 'vite-plugin-md'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
          include: [/\.vue$/, /\.md$/]
    }),
    Markdown({
      builders: []
    })
  ],
  build: {
    rollupOptions:{
      external:['vue'],
      output:{
        globals:{
          vue:'Vue'
        }
      }
    },
    lib:{
      entry: './packages/index.js',
      name: 'ananas-ui',
      fileName: 'ananas-ui'
    }
  }
})
